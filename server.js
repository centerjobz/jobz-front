const express = require('express')
const path = require('path')
const history = require('connect-history-api-fallback')
const morgan = require('morgan')
const app = express()
const port = process.env.PORT || 5000

app.use(morgan('common'))
app.use(history())
app.use(express.static(path.join(__dirname, 'dist')))
app.listen(port, () => {
  console.log(`Server started on port ${port}`)
})
