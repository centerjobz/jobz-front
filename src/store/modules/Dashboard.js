import axios from '@/axios'
import { Notify, Loading, QSpinnerTail } from 'quasar'

export default {
  state: {
    dashboardData: {},
    companyDisable: {},
    searchByDay: {
      labels: [],
      series: []
    },
    userByDay: {
      labels: [],
      series: []
    }
  },
  actions: {
    getDataAnalytcs ({ commit }) {
      axios.get('/platform/dashboard/dashboard')
        .then(res => {
          commit('setDashboardData', res.data)
        })
        .catch(err => {
          console.log(err)
        })
    },
    setTopCompany ({ dispatch }, json) {
      return new Promise((resolve, reject) => {
        axios.post(`/platform/company/${json.id}/top?isTop=${json.top}`)
          .then(res => {
            Notify.create({
              message: 'Status de destaque alterado com sucesso!',
              position: 'top',
              type: 'positive'
            })
            Loading.hide()
            resolve()
          })
          .catch(err => {
            console.log(err)
            Notify.create({
              message: 'Não foi possível mudar o destaque da empresa!',
              position: 'top',
              type: 'negative'
            })
            Loading.hide()
            reject(err)
          })
      })
    },
    getCompanyDisable ({ commit }) {
      axios.get('platform/company/disables')
        .then(res => {
          commit('setCompanyDisable', res.data)
        })
        .catch(err => {
          console.log(err)
        })
    },
    enableCompany ({ dispatch }, id) {
      Loading.show({ spinner: QSpinnerTail, spinnerSize: 80, spinnerColor: 'blue' })
      axios.post(`platform/company/enable/${id}`)
        .then(res => {
          Notify.create({
            message: 'Empresa habilitada com sucesso',
            position: 'top',
            type: 'positive'
          })
          dispatch('getCompanyDisable')
          Loading.hide()
        })
        .catch(err => {
          console.log(err)
          Loading.hide()
        })
    }
  },
  mutations: {
    setDashboardData (state, data) {
      state.dashboardData = data
      data.countFreightSearchByDay.map(element => state.searchByDay.labels.push(element.label.split('-').reverse().join().replace(/,/g, '/')))
      data.countFreightSearchByDay.map(element => state.searchByDay.series.push(element.value))
      // eslint-disable-next-line
      data.countUsersByDay.map(element => state.userByDay.labels.push(element.label === null ? element.label = 'Sem data' : element.label.split('-').reverse().join().replace(/,/g, '/')))
      data.countUsersByDay.map(element => state.userByDay.series.push(element.value))
    },
    setCompanyDisable (state, data) {
      state.companyDisable = []
      state.companyDisable = data
    }
  },
  getters: {
    getDashboardData: state => {
      return state.dashboardData
    },
    getUserByDay: state => {
      return state.userByDay
    },
    getSearchByDay: state => {
      return state.searchByDay
    },
    getCompanyDisable: state => {
      return state.companyDisable
    }
  }
}
