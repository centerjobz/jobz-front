import axios from 'axios'

axios.defaults.baseURL = ''

let userToken = window.localStorage.getItem('userToken')
axios.defaults.headers.common['Token'] = userToken

export default axios
