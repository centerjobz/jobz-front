import Vue from 'vue'
import VueI18n from 'vue-i18n'
import ptBR from './pt-br'
import enUS from './en-us'

Vue.use(VueI18n)

const messages = {
  'en-US': {
    message: enUS
  },
  'pt-BR': {
    message: ptBR
  }
}

export default new VueI18n({
  locale: 'pt-BR',
  messages
})
