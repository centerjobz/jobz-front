import Vue from 'vue'
import Router from 'vue-router'

// Layouts
import Main from './layouts/Main.vue'

// View
import List from './pages/List.vue'
Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Main',
      component: Main
    },
    {
      path: '/list',
      name: 'List',
      component: List
    }
  ]
})

export default router
